﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region privateRegion
        private GameObject currentSelectedObject, grabbedGameObject;
        private bool DiscSelection= true, grabObject = false, gameStarts = false, freeze = true;

        [SerializeField]
        private float rayDistance; // ray Distance

        [SerializeField]
        private LayerMask layerMask; // list of unity layers
    #endregion

    #region publicRegion
        public int discSize, i=0, movesCount = 0;

        public Transform crossHair;
        public Transform[] spawningPoints;

        public Color[] discColor;

        public GameObject[] rods;
        public GameObject curtain, winnerCurtain, ErroBox;
        public List<GameObject> discs = new List<GameObject>();

        public Material[] materials;

        public TextMeshProUGUI movesCountHolder, sliderValueHolder, ErrorMsg, totalsec, totalMove;

        public Slider discSlider;

        public static Stack<GameObject> rod1 = new Stack<GameObject>();
        public static Stack<GameObject> rod2 = new Stack<GameObject>();
        public static Stack<GameObject> rod3 = new Stack<GameObject>();
    #endregion

    #region Regionsingleton
        public static GameManager instance;
    #endregion



    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        discSlider.onValueChanged.AddListener(delegate { sliderValueUpdate(); }); // update slider value on change
    }

    #region GameControlls
        public void StartGame()
        {
            discSize = (int)discSlider.value;
            GameObject DiscObject = Resources.Load<GameObject>("Prefabs/Hanoi/DiscParent");

            /*
             *initialize the disc on the game start
             * assign assets info data for each disc
             * add stack on the rod.
             */
            while (i < discSize)
            {
                DiscObject = Instantiate(DiscObject, spawningPoints[0].position, Quaternion.identity);
                AssetInfo assetInfo = DiscObject.GetComponent<AssetInfo>();
                assetInfo.assetID = discSize - i;
                assetInfo.currentRod = 0;
                DiscObject.transform.position += new Vector3(0, (DiscObject.transform.localScale.y * (i + 1)), 0);
                DiscObject.transform.localScale -= new Vector3((0.05f), 0, (0.05f));
                DiscObject.GetComponent<Renderer>().material.color = discColor[i];
                DiscObject.GetComponent<AssetInfo>().color = discColor[i];
                pushStackObject(0, DiscObject);
                discs.Add(DiscObject);
                i++;
            }

            curtain.gameObject.SetActive(false); // stage curtain off
            discSlider.gameObject.SetActive(false); 
            CameraMovement.instance.StartMovement(); // allow camera Movement
            Timer.instance.StartTimer();
            freeze = false;
        }

        public void ResetGame()
        {
            rod1.Clear();
            rod2.Clear();
            rod3.Clear();
            movesCount = 0;
            movesCountHolder.text = movesCount.ToString();
            Timer.instance.ResetTimer();

            i = 0;
            while (i <discs.Count)
            {
                Destroy(discs[i]);
                i++;
            }

            i = 0;
            DiscSelection = true;
            grabObject = false;
        }

        public void QuitGame()
        {
            ResetGame();
            discSize = 0;
            discSlider.gameObject.SetActive(true);
            winnerCurtain.gameObject.SetActive(false);
            ShowCurtain();
            CameraMovement.instance.StopMovement();
        }
        public void RestartGame()
        {
            movesCount = 0;
            winnerCurtain.gameObject.SetActive(false);
            ResetGame();
            StartGame();
        }
    #endregion

    private void ShowCurtain()
    {
        curtain.gameObject.SetActive(true);
    }

    // shoeing error msg for 3 sec
    IEnumerator ShowError(string msg)
    {
        ErroBox.gameObject.SetActive(true);
        ErrorMsg.text = msg;
        yield return new WaitForSeconds(3f);
        ErroBox.gameObject.SetActive(false);
    }

    public void RemoveCuratin()
    {
        discSize = (int)discSlider.value;
        curtain.gameObject.SetActive(false);
        StartGame();
    }

    public void sliderValueUpdate()
    {
        sliderValueHolder.text = discSlider.value.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        if (freeze)
        {
            CameraMovement.instance.StopMovement();
            return;
        }


        #region MouseClickEvents
            if (Input.GetMouseButtonDown(0))
        {
            if (currentSelectedObject == null)
                return;

            if (currentSelectedObject.GetComponent<AssetInfo>().type == "restart")
            {
                RestartGame();
                return;
            }

            if (currentSelectedObject.GetComponent<AssetInfo>().type == "quit")
            {
                QuitGame();
                return;
            }

            if (grabObject == false)
            {
                AssetInfo assetInfo = currentSelectedObject.GetComponent<AssetInfo>();
                GameObject peekObject = PeekRods(assetInfo.currentRod);

                if (peekObject.GetComponent<AssetInfo>().assetID == assetInfo.assetID)
                {
                    grabObject = true;
                    movesCount += 1;
                    movesCountHolder.text = "moves: " + movesCount;
                    DiscSelection = false;

                    //lastKnownPosition = currentSelectedObject.transform.position;
                    grabbedGameObject = currentSelectedObject;
                    grabbedGameObject.transform.GetComponent<Collider>().enabled = false;
                    StartCoroutine(SmoothMove(grabbedGameObject.transform, Camera.main.ScreenToWorldPoint(new Vector3(crossHair.position.x, crossHair.position.y, .7f)), 1f));
                }

                else
                {
                    StartCoroutine(ShowError("Only you can pick the top Disc"));
                }
            }
            else
                MoveDiscBegin();

        }
        #endregion

        #region RayCast Events
            Ray ray = Camera.main.GetComponentInChildren<Camera>().ScreenPointToRay(new Vector2(crossHair.position.x, crossHair.position.y));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayDistance, layerMask)) //layermask determindes what can you click on
            {
                if (hit.transform.GetComponent<AssetInfo>().type == "restart" || hit.transform.GetComponent<AssetInfo>().type == "quit")
                {
                    ObjectSelected(hit.transform);
                    return;
                }

                if (DiscSelection && hit.transform.GetComponent<AssetInfo>().type == "disc")
                    ObjectSelected(hit.transform);

                else if (!DiscSelection && hit.transform.GetComponent<AssetInfo>().type == "rod")
                    ObjectSelected(hit.transform);

                else
                    ClearSelected();

            }
            else
            {
                ClearSelected();
            }
        #endregion

        // object in the player hand
        if (grabObject)
        {
            grabbedGameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(crossHair.position.x, crossHair.position.y, .7f));
        }
    }

    #region Disc Movement
        private void MoveDiscBegin()
        {
            if(currentSelectedObject.GetComponent<AssetInfo>().currentRod == grabbedGameObject.GetComponent<AssetInfo>().currentRod)
            {
                MoveDisc(currentSelectedObject.GetComponent<AssetInfo>().currentRod, currentSelectedObject.GetComponent<AssetInfo>().currentRod);
                return;
            }
            else if (CheckPossibleMoves(currentSelectedObject.GetComponent<AssetInfo>().currentRod)) // check moves Values
            {
                MoveDisc(grabbedGameObject.GetComponent<AssetInfo>().currentRod, currentSelectedObject.GetComponent<AssetInfo>().currentRod);
            }
            else
            {
                StartCoroutine(ShowError("You can't Place Here."));
            }
        }

        private void MoveDisc(int current, int target)
        {
            int count = getStackCount(target);

            if(current == target) {
                MovingTowardsSpawningPoints(target, count -1);
                return;
            }

            popStackCount(current);
            pushStackObject(target, grabbedGameObject);
            MovingTowardsSpawningPoints(target, count);

        }

        private void MovingTowardsSpawningPoints(int target, int count)
    {
        StartCoroutine(SmoothMove(grabbedGameObject.transform, spawningPoints[target].position, 3f, grabbedGameObject.transform.localScale.y * (count + 1)));
        grabbedGameObject.GetComponent<AssetInfo>().currentRod = target;
        grabbedGameObject.GetComponent<Collider>().enabled = true;
        grabObject = false;
        DiscSelection = true;
        grabbedGameObject = null;
        ClearSelected();

        if (getStackCount(target) == discSize && target != 0)
            Winner();
    }

        IEnumerator SmoothMove(Transform objectToMove, Vector3 targetPosition, float speed, float height = 0)
    {
        Vector3 StratingPostion = objectToMove.position;
        float step = (speed / (StratingPostion - targetPosition).magnitude) * Time.fixedDeltaTime;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step;
            objectToMove.position = Vector3.Lerp(StratingPostion, targetPosition, t);
            yield return new WaitForFixedUpdate();// Leave the routine and return here in the next frame
        }

        objectToMove.position = targetPosition;

        if (height > 0)
            objectToMove.position += new Vector3(0, height, 0);
    }

        private bool CheckPossibleMoves(int index)
    {

        if (getStackCount(index) < 1)
            return true;
        else
        {

            if (PeekRods(index).GetComponent<AssetInfo>().assetID > grabbedGameObject.GetComponent<AssetInfo>().assetID)
                return true;
            else
                return false;
        }
    }
    #endregion

    #region Stack Manipulation
    private void pushStackObject(int target, GameObject nextObject)
        {
            switch (target)
            {
                case 0:
                    rod1.Push(nextObject);
                    break;
                case 1:
                    rod2.Push(nextObject);
                    break;
                default:
                    rod3.Push(nextObject);
                    break;
            }
        }

        private void popStackCount(int target)
        {
            switch (target)
            {
                case 0:
                     rod1.Pop();
                    break;
                case 1:
                    rod2.Pop();
                    break;
                default:
                    rod3.Pop();
                    break;
            }
        }

        private int getStackCount(int target)
        {
            switch (target)
            {
                case 0:
                    return rod1.Count;
                case 1:
                    return rod2.Count;
                default:
                    return rod3.Count;
            }
        }

        private GameObject PeekRods(int target)
        {
            switch (target)
            {
                case 0:
                    return rod1.Peek();
                case 1:
                    return rod2.Peek();
                default:
                    return rod3.Peek();
            }
        }
    #endregion

    // annoucing winner Banner
    private void Winner()
    {
        CameraMovement.instance.StopMovement();
        Timer.instance.StopTimer();
        freeze = false;
        winnerCurtain.gameObject.SetActive(true);
        totalMove.text = movesCount.ToString();
        totalsec.text = Timer.instance.timerText.text;
    }

    // After Ray selected Object
    private void ObjectSelected(Transform hit)
    {
        if (currentSelectedObject != null)
            ClearSelected();

        hit.transform.GetComponent<AssetInfo>().selected = true;
        hit.transform.GetComponent<Renderer>().material = materials[1];
        currentSelectedObject = hit.transform.gameObject;
    }

    // Clear selection on the object
    private void ClearSelected()
    {
        if (currentSelectedObject == null)
            return;

        if (currentSelectedObject.transform.GetComponent<AssetInfo>().type == "rod")
            currentSelectedObject.transform.GetComponent<Renderer>().material = materials[2];

        else if (currentSelectedObject.transform.GetComponent<AssetInfo>().type == "restart" || currentSelectedObject.transform.GetComponent<AssetInfo>().type == "quit")
            currentSelectedObject.transform.GetComponent<Renderer>().material = materials[3];

        else
        {
            currentSelectedObject.transform.GetComponent<Renderer>().material = materials[0];
            currentSelectedObject.GetComponent<Renderer>().material.color = currentSelectedObject.GetComponent<AssetInfo>().color;
        }

        currentSelectedObject.GetComponent<AssetInfo>().selected = false;
        currentSelectedObject = null;
    }
}
