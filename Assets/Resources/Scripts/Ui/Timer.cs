﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    #region privateRegion
        private float timer; // hold the time
        private bool gameStarts = false;
    #endregion

    #region publicRegion
        public static Timer instance;
        public TextMeshProUGUI timerText;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        timerText = GetComponent<TextMeshProUGUI>(); // timer UI Holder
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStarts)
        {
            timer += Time.deltaTime;
            timerText.text = timer.ToString("F");
        }
    }

    public void StartTimer()
    {
        gameStarts = true;
    }

    public void StopTimer()
    {
        gameStarts = false;
    }

    // reset to 0
    public void ResetTimer()
    {
        gameStarts = false;
        timer = 0;
        timerText.text = "";
    }
}
