﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    #region privateRegion
        private Vector2 currentRotation, playerRotation;

        [SerializeField] // to visible on the Inspector
        private float sensitivity;

        [SerializeField] // to visible on the Inspector
        private float maxYAngle = 80f;

        [SerializeField]
        private bool gameStarts = false;
    #endregion

    #region publicRegion
        public static CameraMovement instance;
    #endregion

    void Start()
    {
        instance = this;
    }

    public void StartMovement()
    {
        gameStarts = true;
    }

    public void StopMovement()
    {
        gameStarts = false;
    }

    void Update()
    {
        if (gameStarts == false)
            return;

        #region Movement
            float horizontalInput = Input.GetAxis("Horizontal"); // left and right
            float verticalInput = Input.GetAxis("Vertical");  // Foeward Backward

            //update the Player Position
            if(verticalInput > 0)
                transform.Translate(0.0f, 0.0f, 5 * Time.deltaTime);

            if (verticalInput < 0)
                transform.Translate(0.0f, 0.0f, -5 * Time.deltaTime);

            if (horizontalInput > 0)
                transform.Translate(5 * Time.deltaTime, 0.0f, 0.0f);

            if (horizontalInput < 0)
                transform.Translate(-5 * Time.deltaTime, 0.0f, 0.0f);

        #endregion

        #region Rotation
            currentRotation.x += Input.GetAxis("Mouse X") * sensitivity;
            playerRotation.x += Input.GetAxis("Mouse X") * sensitivity;
            currentRotation.y -= Input.GetAxis("Mouse Y") * sensitivity;
            currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle); // restriction on rotation

        transform.rotation = Quaternion.Euler(0, playerRotation.x, 0); // update the player Roatation
        Camera.main.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);// update the camera rotation

        #endregion
    }
}
