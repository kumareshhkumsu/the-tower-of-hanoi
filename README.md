# Inspirit Challenge #

# The Tower of Hanoi #

The Tower of Hanoi is a famous mathematical game. It consists of three rods and a
number of disks of different sizes, which can slide onto any rod. The puzzle starts with the
disks in a neat stack in ascending order of size on one rod, the smallest at the top, thus
making a conical shape.

* Version 1.0.0

### Unity ###

* version -> 2020.1.2f1
* Scene -> GameScene
* Graphics -> Universal RP 

### Unity Packages Installed ###

* TextMeshPro
* Universal RP
* Shader Graph

### Rules ###

* Only one disc can be moved at a time.
* Each move consists of taking the topmost disc from a stack and moving it to the top of another stack.
* A larger diameter disc may never be placed on a smaller disc.

### Demo ###

* [Quick GamePlay Video](https://drive.google.com/file/d/1lI9lnhBNILu4ogsm-mJikieqhcnw3HK1/view)
* [Live Demo](https://20cwizards.com/TowerOfHanoi)

### Owned By ###

* Author Kumaresan V
* [www.20cwizards.com](https://20cwizards.com/)